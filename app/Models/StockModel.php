<?php

namespace App\Models;

use CodeIgniter\Model;

class StockModel extends Model
{
    protected $table            = 'md_stock';
    protected $primaryKey       = 'id_product';
    protected $allowedFields    = ['id_product','nm_product','price','stock'];
}
